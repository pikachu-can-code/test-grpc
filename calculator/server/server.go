package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"math"
	"net"
	"net/http"
	"test-grpc/calculator"
	"test-grpc/calculator/server/multiplexer"
	"time"

	"github.com/improbable-eng/grpc-web/go/grpcweb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

type server struct {
	calculator.CalculatorServiceServer
}

func (*server) Sum(ctx context.Context, req *calculator.SumRequest) (*calculator.SumRespons, error) {
	fmt.Println("sum called")
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, status.Errorf(codes.InvalidArgument, "Expect num > 0")
	}
	headers := md["authorization"]
	log.Println(headers)
	resp := &calculator.SumRespons{
		Sum: req.GetNum1() + req.GetNum2(),
	}
	return resp, nil
}

func (*server) PrimeNumber(req *calculator.PNDRequest, stream calculator.CalculatorService_PrimeNumberServer) error {
	fmt.Println("PrimeNumber called")
	k := int32(2)
	N := req.GetNumber()
	for N > 1 {
		if N%k == 0 {
			N = N / k
			// send to client
			stream.Send(&calculator.PNDResponse{
				Result: k,
			})
			time.Sleep(1 * time.Second)
		} else {
			k++
		}
	}
	return nil
}

func (*server) Max(stream calculator.CalculatorService_MaxServer) error {
	fmt.Println("Find max called")
	var max int32
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			fmt.Println("client finished streaming")
			return nil
		}
		if err != nil {
			log.Fatalf("error: %v", err)
			return err
		}

		num := req.GetNum()
		fmt.Println("server get num = ", num)
		if num > max {
			max = num
		}
		err = stream.Send(&calculator.FindMaxResponse{
			Max: max,
		})
		if err != nil {
			log.Fatalf("error: %v", err)
			return err
		}
	}
}

func (*server) Square(c context.Context, req *calculator.SquareRequest) (*calculator.SquareResponse, error) {
	fmt.Println("square called")
	num := req.GetNum()
	if num < 0 {
		return nil, status.Errorf(codes.InvalidArgument, "Expect num > 0")
	}
	for i := 0; i < 5; i++ {
		if c.Err() == context.Canceled {
			log.Println("canceled....")
			return nil, nil
		}
		time.Sleep(3 * time.Second)
	}

	return &calculator.SquareResponse{
		Root: int32(math.Sqrt(float64(num))),
	}, nil
}

func (*server) Average(stream calculator.CalculatorService_AverageServer) error {
	fmt.Println("Average called")
	var total float32
	var count int
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			resp := &calculator.AVGResponse{
				Result: total / float32(count),
			}
			return stream.SendAndClose(resp)
		}
		if err != nil {
			log.Fatalf("error: %v", err)
		}

		total += req.GetNum()
		count++
	}
}

func main() {
	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalf("Server start with error: %v", err)
	}

	s := grpc.NewServer()
	calculator.RegisterCalculatorServiceServer(s, &server{})

	go func() {
		fmt.Println("Server start in port 8080")
		log.Fatal(s.Serve(lis))
	}()
	grpcWebServer := grpcweb.WrapServer(s)
	multiplex := multiplexer.GrpcMultiplexer{
		WrappedGrpcServer: grpcWebServer,
	}
	r := http.NewServeMux()
	webapp := http.FileServer(http.Dir("ui"))
	r.Handle("/", multiplex.Handler(webapp))
	srv := &http.Server{
		Handler: r,
		Addr:    "localhost:9090",
	}
	err = srv.ListenAndServe()
	if err != nil {
		log.Fatal(err)
	}
}
