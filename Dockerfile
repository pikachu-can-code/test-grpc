FROM golang:1.16 AS builder

RUN mkdir -p /app

WORKDIR /app

COPY . .

RUN mkdir -p bin
RUN go build -o ./bin/main ./calculator/server/server.go

CMD [ "/app/bin/main" ]
