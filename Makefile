gen-calculator:
	protoc calculator/calculator.proto --go-grpc_out=. --go-grpc_opt=paths=source_relative --go_out=. --go_opt=paths=source_relative --grpc-gateway_out=logtostderr=true,paths=source_relative:. --grpc-gateway-cors_out=.    
gen-contact:
	protoc contact/contact.proto --go_out=plugins=grpc:.