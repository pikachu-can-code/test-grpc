/**
 * @fileoverview gRPC-Web generated client stub for calculator
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.calculator = require('./calculator_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.calculator.CalculatorServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.calculator.CalculatorServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.calculator.SumRequest,
 *   !proto.calculator.SumRespons>}
 */
const methodDescriptor_CalculatorService_Sum = new grpc.web.MethodDescriptor(
  '/calculator.CalculatorService/Sum',
  grpc.web.MethodType.UNARY,
  proto.calculator.SumRequest,
  proto.calculator.SumRespons,
  /**
   * @param {!proto.calculator.SumRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.calculator.SumRespons.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.calculator.SumRequest,
 *   !proto.calculator.SumRespons>}
 */
const methodInfo_CalculatorService_Sum = new grpc.web.AbstractClientBase.MethodInfo(
  proto.calculator.SumRespons,
  /**
   * @param {!proto.calculator.SumRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.calculator.SumRespons.deserializeBinary
);


/**
 * @param {!proto.calculator.SumRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.calculator.SumRespons)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.calculator.SumRespons>|undefined}
 *     The XHR Node Readable Stream
 */
proto.calculator.CalculatorServiceClient.prototype.sum =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/calculator.CalculatorService/Sum',
      request,
      metadata || {},
      methodDescriptor_CalculatorService_Sum,
      callback);
};


/**
 * @param {!proto.calculator.SumRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.calculator.SumRespons>}
 *     Promise that resolves to the response
 */
proto.calculator.CalculatorServicePromiseClient.prototype.sum =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/calculator.CalculatorService/Sum',
      request,
      metadata || {},
      methodDescriptor_CalculatorService_Sum);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.calculator.PNDRequest,
 *   !proto.calculator.PNDResponse>}
 */
const methodDescriptor_CalculatorService_PrimeNumber = new grpc.web.MethodDescriptor(
  '/calculator.CalculatorService/PrimeNumber',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.calculator.PNDRequest,
  proto.calculator.PNDResponse,
  /**
   * @param {!proto.calculator.PNDRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.calculator.PNDResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.calculator.PNDRequest,
 *   !proto.calculator.PNDResponse>}
 */
const methodInfo_CalculatorService_PrimeNumber = new grpc.web.AbstractClientBase.MethodInfo(
  proto.calculator.PNDResponse,
  /**
   * @param {!proto.calculator.PNDRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.calculator.PNDResponse.deserializeBinary
);


/**
 * @param {!proto.calculator.PNDRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.calculator.PNDResponse>}
 *     The XHR Node Readable Stream
 */
proto.calculator.CalculatorServiceClient.prototype.primeNumber =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/calculator.CalculatorService/PrimeNumber',
      request,
      metadata || {},
      methodDescriptor_CalculatorService_PrimeNumber);
};


/**
 * @param {!proto.calculator.PNDRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.calculator.PNDResponse>}
 *     The XHR Node Readable Stream
 */
proto.calculator.CalculatorServicePromiseClient.prototype.primeNumber =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/calculator.CalculatorService/PrimeNumber',
      request,
      metadata || {},
      methodDescriptor_CalculatorService_PrimeNumber);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.calculator.SquareRequest,
 *   !proto.calculator.SquareResponse>}
 */
const methodDescriptor_CalculatorService_Square = new grpc.web.MethodDescriptor(
  '/calculator.CalculatorService/Square',
  grpc.web.MethodType.UNARY,
  proto.calculator.SquareRequest,
  proto.calculator.SquareResponse,
  /**
   * @param {!proto.calculator.SquareRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.calculator.SquareResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.calculator.SquareRequest,
 *   !proto.calculator.SquareResponse>}
 */
const methodInfo_CalculatorService_Square = new grpc.web.AbstractClientBase.MethodInfo(
  proto.calculator.SquareResponse,
  /**
   * @param {!proto.calculator.SquareRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.calculator.SquareResponse.deserializeBinary
);


/**
 * @param {!proto.calculator.SquareRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.calculator.SquareResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.calculator.SquareResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.calculator.CalculatorServiceClient.prototype.square =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/calculator.CalculatorService/Square',
      request,
      metadata || {},
      methodDescriptor_CalculatorService_Square,
      callback);
};


/**
 * @param {!proto.calculator.SquareRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.calculator.SquareResponse>}
 *     Promise that resolves to the response
 */
proto.calculator.CalculatorServicePromiseClient.prototype.square =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/calculator.CalculatorService/Square',
      request,
      metadata || {},
      methodDescriptor_CalculatorService_Square);
};


module.exports = proto.calculator;

