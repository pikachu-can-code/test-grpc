import logo from "./logo.svg";
import "./App.css";
import React from "react";
import { CalculatorServiceClient } from "./lib/calculator/calculator_grpc_web_pb";
import { SumRequest, PNDRequest } from "./lib/calculator/calculator_pb";
import * as grpcWeb from "grpc-web";
// import axios from "axios";

export default function App() {
  React.useEffect(() => {
    // axios
    //   .post("http://127.0.0.1:8081/v1/prime", { number: 120 })
    //   .then((res) => {
    //     console.log(res);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });
    const client = new CalculatorServiceClient(
      "http://test.unigwork.com",
      null,
      null
    );
    const requestSum = new SumRequest();
    requestSum.setNum1(1);
    requestSum.setNum2(5);
    var meta = {
      authorization: "alo",
    };
    client.sum(requestSum, meta, (err, resp) => {
      if (err !== null) {
        console.error(err);
        console.log(resp);
      } else {
        console.log(resp.getSum());
      }
    });

    const pndRequest = new PNDRequest();
    pndRequest.setNumber(120);
    let stream = client.primeNumber(pndRequest, null);
    stream.on("data", (resp) => {
      console.log(resp.getResult());
    });
  }, []);
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}
