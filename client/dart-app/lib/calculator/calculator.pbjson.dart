///
//  Generated code. Do not modify.
//  source: calculator/calculator.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use sumRequestDescriptor instead')
const SumRequest$json = const {
  '1': 'SumRequest',
  '2': const [
    const {'1': 'num1', '3': 1, '4': 1, '5': 5, '10': 'num1'},
    const {'1': 'num2', '3': 2, '4': 1, '5': 5, '10': 'num2'},
  ],
};

/// Descriptor for `SumRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List sumRequestDescriptor = $convert.base64Decode('CgpTdW1SZXF1ZXN0EhIKBG51bTEYASABKAVSBG51bTESEgoEbnVtMhgCIAEoBVIEbnVtMg==');
@$core.Deprecated('Use sumResponsDescriptor instead')
const SumRespons$json = const {
  '1': 'SumRespons',
  '2': const [
    const {'1': 'sum', '3': 1, '4': 1, '5': 5, '10': 'sum'},
  ],
};

/// Descriptor for `SumRespons`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List sumResponsDescriptor = $convert.base64Decode('CgpTdW1SZXNwb25zEhAKA3N1bRgBIAEoBVIDc3Vt');
@$core.Deprecated('Use pNDRequestDescriptor instead')
const PNDRequest$json = const {
  '1': 'PNDRequest',
  '2': const [
    const {'1': 'number', '3': 1, '4': 1, '5': 5, '10': 'number'},
  ],
};

/// Descriptor for `PNDRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List pNDRequestDescriptor = $convert.base64Decode('CgpQTkRSZXF1ZXN0EhYKBm51bWJlchgBIAEoBVIGbnVtYmVy');
@$core.Deprecated('Use pNDResponseDescriptor instead')
const PNDResponse$json = const {
  '1': 'PNDResponse',
  '2': const [
    const {'1': 'result', '3': 1, '4': 1, '5': 5, '10': 'result'},
  ],
};

/// Descriptor for `PNDResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List pNDResponseDescriptor = $convert.base64Decode('CgtQTkRSZXNwb25zZRIWCgZyZXN1bHQYASABKAVSBnJlc3VsdA==');
@$core.Deprecated('Use aVGRequestDescriptor instead')
const AVGRequest$json = const {
  '1': 'AVGRequest',
  '2': const [
    const {'1': 'num', '3': 1, '4': 1, '5': 2, '10': 'num'},
  ],
};

/// Descriptor for `AVGRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List aVGRequestDescriptor = $convert.base64Decode('CgpBVkdSZXF1ZXN0EhAKA251bRgBIAEoAlIDbnVt');
@$core.Deprecated('Use aVGResponseDescriptor instead')
const AVGResponse$json = const {
  '1': 'AVGResponse',
  '2': const [
    const {'1': 'result', '3': 1, '4': 1, '5': 2, '10': 'result'},
  ],
};

/// Descriptor for `AVGResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List aVGResponseDescriptor = $convert.base64Decode('CgtBVkdSZXNwb25zZRIWCgZyZXN1bHQYASABKAJSBnJlc3VsdA==');
@$core.Deprecated('Use findMaxRequestDescriptor instead')
const FindMaxRequest$json = const {
  '1': 'FindMaxRequest',
  '2': const [
    const {'1': 'num', '3': 1, '4': 1, '5': 5, '10': 'num'},
  ],
};

/// Descriptor for `FindMaxRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List findMaxRequestDescriptor = $convert.base64Decode('Cg5GaW5kTWF4UmVxdWVzdBIQCgNudW0YASABKAVSA251bQ==');
@$core.Deprecated('Use findMaxResponseDescriptor instead')
const FindMaxResponse$json = const {
  '1': 'FindMaxResponse',
  '2': const [
    const {'1': 'max', '3': 1, '4': 1, '5': 5, '10': 'max'},
  ],
};

/// Descriptor for `FindMaxResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List findMaxResponseDescriptor = $convert.base64Decode('Cg9GaW5kTWF4UmVzcG9uc2USEAoDbWF4GAEgASgFUgNtYXg=');
@$core.Deprecated('Use squareRequestDescriptor instead')
const SquareRequest$json = const {
  '1': 'SquareRequest',
  '2': const [
    const {'1': 'num', '3': 1, '4': 1, '5': 5, '10': 'num'},
  ],
};

/// Descriptor for `SquareRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List squareRequestDescriptor = $convert.base64Decode('Cg1TcXVhcmVSZXF1ZXN0EhAKA251bRgBIAEoBVIDbnVt');
@$core.Deprecated('Use squareResponseDescriptor instead')
const SquareResponse$json = const {
  '1': 'SquareResponse',
  '2': const [
    const {'1': 'root', '3': 1, '4': 1, '5': 5, '10': 'root'},
  ],
};

/// Descriptor for `SquareResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List squareResponseDescriptor = $convert.base64Decode('Cg5TcXVhcmVSZXNwb25zZRISCgRyb290GAEgASgFUgRyb290');
