///
//  Generated code. Do not modify.
//  source: calculator/calculator.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'calculator.pb.dart' as $0;
export 'calculator.pb.dart';

class CalculatorServiceClient extends $grpc.Client {
  static final _$sum = $grpc.ClientMethod<$0.SumRequest, $0.SumRespons>(
      '/calculator.CalculatorService/Sum',
      ($0.SumRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.SumRespons.fromBuffer(value));
  static final _$primeNumber =
      $grpc.ClientMethod<$0.PNDRequest, $0.PNDResponse>(
          '/calculator.CalculatorService/PrimeNumber',
          ($0.PNDRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.PNDResponse.fromBuffer(value));
  static final _$average = $grpc.ClientMethod<$0.AVGRequest, $0.AVGResponse>(
      '/calculator.CalculatorService/Average',
      ($0.AVGRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.AVGResponse.fromBuffer(value));
  static final _$max =
      $grpc.ClientMethod<$0.FindMaxRequest, $0.FindMaxResponse>(
          '/calculator.CalculatorService/Max',
          ($0.FindMaxRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.FindMaxResponse.fromBuffer(value));
  static final _$square =
      $grpc.ClientMethod<$0.SquareRequest, $0.SquareResponse>(
          '/calculator.CalculatorService/Square',
          ($0.SquareRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.SquareResponse.fromBuffer(value));

  CalculatorServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.SumRespons> sum($0.SumRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$sum, request, options: options);
  }

  $grpc.ResponseStream<$0.PNDResponse> primeNumber($0.PNDRequest request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(
        _$primeNumber, $async.Stream.fromIterable([request]),
        options: options);
  }

  $grpc.ResponseFuture<$0.AVGResponse> average(
      $async.Stream<$0.AVGRequest> request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(_$average, request, options: options).single;
  }

  $grpc.ResponseStream<$0.FindMaxResponse> max(
      $async.Stream<$0.FindMaxRequest> request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(_$max, request, options: options);
  }

  $grpc.ResponseFuture<$0.SquareResponse> square($0.SquareRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$square, request, options: options);
  }
}

abstract class CalculatorServiceBase extends $grpc.Service {
  $core.String get $name => 'calculator.CalculatorService';

  CalculatorServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.SumRequest, $0.SumRespons>(
        'Sum',
        sum_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.SumRequest.fromBuffer(value),
        ($0.SumRespons value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PNDRequest, $0.PNDResponse>(
        'PrimeNumber',
        primeNumber_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $0.PNDRequest.fromBuffer(value),
        ($0.PNDResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.AVGRequest, $0.AVGResponse>(
        'Average',
        average,
        true,
        false,
        ($core.List<$core.int> value) => $0.AVGRequest.fromBuffer(value),
        ($0.AVGResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.FindMaxRequest, $0.FindMaxResponse>(
        'Max',
        max,
        true,
        true,
        ($core.List<$core.int> value) => $0.FindMaxRequest.fromBuffer(value),
        ($0.FindMaxResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SquareRequest, $0.SquareResponse>(
        'Square',
        square_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.SquareRequest.fromBuffer(value),
        ($0.SquareResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.SumRespons> sum_Pre(
      $grpc.ServiceCall call, $async.Future<$0.SumRequest> request) async {
    return sum(call, await request);
  }

  $async.Stream<$0.PNDResponse> primeNumber_Pre(
      $grpc.ServiceCall call, $async.Future<$0.PNDRequest> request) async* {
    yield* primeNumber(call, await request);
  }

  $async.Future<$0.SquareResponse> square_Pre(
      $grpc.ServiceCall call, $async.Future<$0.SquareRequest> request) async {
    return square(call, await request);
  }

  $async.Future<$0.SumRespons> sum(
      $grpc.ServiceCall call, $0.SumRequest request);
  $async.Stream<$0.PNDResponse> primeNumber(
      $grpc.ServiceCall call, $0.PNDRequest request);
  $async.Future<$0.AVGResponse> average(
      $grpc.ServiceCall call, $async.Stream<$0.AVGRequest> request);
  $async.Stream<$0.FindMaxResponse> max(
      $grpc.ServiceCall call, $async.Stream<$0.FindMaxRequest> request);
  $async.Future<$0.SquareResponse> square(
      $grpc.ServiceCall call, $0.SquareRequest request);
}
